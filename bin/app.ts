import * as cdk from "aws-cdk-lib";
import { CdkGlueEtlStack } from "../lib/cdk-glue-etl-stack";

const app = new cdk.App();
new CdkGlueEtlStack(app, "sample-etl-stack-ppwdr", {});
