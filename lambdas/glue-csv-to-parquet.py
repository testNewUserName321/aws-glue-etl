from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.utils import getResolvedOptions
import sys

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)

# Retrieve passed in parameters
args = getResolvedOptions(sys.argv,
                          ['JOB_NAME',
                           's3_input_file_path',
                           's3_parquet_output_folder'])
input_loc = args['s3_input_file_path']
output_loc = args['s3_parquet_output_folder']

inputDyf = glueContext.create_dynamic_frame_from_options(
    connection_type="s3",
    connection_options={
        "paths": [input_loc]
    },
    format="csv",
    format_options={
        "withHeader": True,
        "separator": ","
    })

outputDF = glueContext.write_dynamic_frame.from_options(
    frame=inputDyf,
    connection_type="s3",
    connection_options={"path": output_loc
        },
    format="parquet")
