## This Project

This project deploys a sample extract, transform, load (ETL) solution using AWS Cloud Development Kit (CDK). An ETL is a common data
integration process that is used to extract data from a source system, transform the data into a format that is more
useful for analysis, and load the data into a data warehouse or data lake. This project deploys a sample ETL solution
and can be used as a building block for any ETL.

## Architecture overview

This project is built on AWS CDK and utilizes Amazon S3, AWS Lambda, and AWS Glue to extract data from an Amazon S3 bucket,
transform the data into a Parquet file, and load the data into an AWS Glue table. You can view the sample data in AWS Athena.

![architecture](https://d2gld5r7icon5i.cloudfront.net/etl.png)

For more information about ETLs, see the [AWS Glue Getting Started Guide](https://aws.amazon.com/glue/getting-started/)

## Connections and permissions

This project supports the Amazon CodeCatalyst Development Role, which can be created from the [AWS management console Codecatalyst application](https://us-west-2.console.aws.amazon.com/codecatalyst/home?region=us-west-2#/). When clicking “add IAM role”, the first option is to create a CodeCatalyst development role. After clicking create, the role will be automatically added to the Amazon CodeCatalyst space. 

The other option is creating a project specific IAM role, which can be added to the Amazon CodeCatalyst space by selecting "Add an existing IAM role" from the add IAM role options. The IAM role needs to contain the CodeCatalyst trust policy, as well as the following permissions:

### Amazon CodeCatalyst role trust policy

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": ["codecatalyst-runner.amazonaws.com", "codecatalyst.amazonaws.com"]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```

### AWS CDK bootstrap and deployment inline policy

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "cloudformation:DescribeStackEvents",
        "cloudformation:DeleteStack",
        "cloudformation:CreateChangeSet",
        "cloudformation:DescribeChangeSet",
        "cloudformation:ExecuteChangeSet",
        "cloudformation:DescribeStacks",
        "cloudformation:GetTemplate",
        "cloudformation:ListStackResources"
      ],
      "Effect": "Allow",
      "Resource": ["arn:aws:cloudformation:*:*:*"]
    },
    {
      "Action": ["sts:AssumeRole"],
      "Effect": "Allow",
      "Resource": ["arn:aws:iam::*:role/*"]
    },
    {
      "Action": [
        "iam:GetRole",
        "iam:UntagRole",
        "iam:GetPolicy",
        "iam:TagRole",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:AttachRolePolicy",
        "iam:PutRolePolicy",
        "iam:TagPolicy",
        "iam:DetachRolePolicy",
        "iam:DeleteRolePolicy",
        "iam:UntagPolicy",
        "iam:UpdateRole",
        "iam:GetRolePolicy"
      ],
      "Effect": "Allow",
      "Resource": ["arn:aws:iam::*:policy/*", "arn:aws:iam::*:role/cdk-*"]
    },
    {
      "Action": [
        "s3:PutEncryptionConfiguration",
        "s3:PutBucketPublicAccessBlock",
        "s3:PutBucketPolicy",
        "s3:CreateBucket",
        "s3:DeleteBucketPolicy",
        "s3:DeleteBucket",
        "s3:GetBucketPolicy",
        "s3:PutBucketVersioning"
      ],
      "Effect": "Allow",
      "Resource": ["arn:aws:s3:::cdk-*"]
    },
    {
      "Action": ["ssm:PutParameter", "ssm:DeleteParameter", "ssm:GetParameters", "ssm:GetParameter"],
      "Effect": "Allow",
      "Resource": ["arn:aws:ssm:*:*:parameter/cdk-bootstrap/*"]
    },
    {
      "Action": ["ecr:DeleteRepository", "ecr:DescribeRepositories", "ecr:SetRepositoryPolicy"],
      "Effect": "Allow",
      "Resource": ["arn:aws:ecr:*:*:repository/cdk-*"]
    },
    {
      "Action": ["ecr:CreateRepository"],
      "Effect": "Allow",
      "Resource": ["*"]
    }
  ]
}
```

## Project resources

The project contains three main folders:

- `lambdas` - contains the code for the AWS Lambda functions that are deployed as part of the AWS CDK stack.
- `lib` - contains the AWS CDK stack code.
- `sample-data` - contains sample data that can be used to test the ETL process.

The AWS CDK application deploys the following resources:

- **Amazon S3 bucket -** The Amazon S3 bucket is used to store the comma separated values (CSV) data that will be converted to Parquet and the AWS Lambda code.
- **AWS Lambda -** The following AWS Lambdas are created as part of the ETL solution:
  - **s3-notify-glue.py -** This AWS Lambda is automatically triggered when a CSV file is uploaded to the "csv-input" subdirectory of the created Amazon S3 bucket. It triggers an AWS Glue job to process the file.
  - **glue-csv-to-parquet.py -** Invoked by the AWS Glue job, this AWS Lambda converts CSV files to Parquet and saves them to the "parquet-output" folder within the created Amazon S3 bucket.
- **AWS Glue job -** An AWS Glue job is created to convert the CSV files to Parquet and is triggered by the "s3-notify-glue.py" AWS Lambda.
- **Amazon Glue table -** An AWS Glue table is created to reference the Parquet files created by the AWS Glue job.
- **AWS Athena query -** An AWS Athena query is created to query the AWS Glue table.

The Amazon CodeCatalyst workflow contains the following actions:

- **CDK Bootstrap** - bootstraps the AWS CDK environment within the provided AWS account and Region. If already bootstrapped, this step is skipped.
  For more information, see the [AWS CDK Bootstrapping Guide](https://docs.aws.amazon.com/cdk/v2/guide/bootstrapping.html)
- **CDK Deploy** - Deploys the AWS CDK application to the provided AWS account and Region. For more information, see the [AWS CDK Deploying Guide](https://docs.aws.amazon.com/cdk/latest/guide/cli.html#cli-deploy)

After being created, this project deploys the following AWS resources:

- AWS Lambda function(s) - A resource to invoke your code on a high-availability compute infrastructure without provisioning or managing servers. For more information on AWS Lambda, see the [AWS Lambda Developer Guide](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
- IAM role(s) - A resource for securely controlled access to AWS resources such as the AWS Lambda function(s). For more information on IAM, see the [AWS IAM User Guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
- Amazon S3 bucket(s) - A resource for storing data. For more information on Amazon S3, see the [AWS S3 Developer Guide](https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html)
- AWS Glue job(s) - A resource for running ETL jobs on AWS. For more information on AWS Glue, see the [AWS Glue Developer Guide](https://docs.aws.amazon.com/glue/latest/dg/welcome.html)
- AWS Athena workgroup(s) - A resource for running queries on AWS. For more information on AWS Athena, see the [AWS Athena Developer Guide](https://docs.aws.amazon.com/athena/latest/ug/what-is.html)
- AWS Athena query(s) - A resource for running queries on AWS. For more information on AWS Athena, see the [AWS Athena Developer Guide](https://docs.aws.amazon.com/athena/latest/ug/what-is.html)

You can view the deployment status in the project's workflow.

## Additional resources

See the Amazon CodeCatalyst User Guide for additional information on using the features and resources of Amazon CodeCatalyst.

## Testing the ETL

This solution automatically creates everything needed to run the ETL process. To test the ETL process, follow the steps below:

1. Download the sample CSV file from within the project's source repository. The file is located at: _sample-data/taxi_data.csv_.
2. Upload the CSV file to the Amazon S3 bucket that was created by the solution. The bucket name can be found in the project's workflow.
   Upload the file to the _csv-input_ folder within the bucket.
3. After the file has been uploaded, Amazon S3 automatically initiates an AWS Glue job using the "s3-notify-glue" AWS Lambda.
4. The AWS Glue job will converts the CSV file to a Parquet file and stores it within the _parquet-output_ folder inside the Amazon S3 bucket.
5. Once the AWS Glue job completes, you can view the data within the AWS Glue table that was created by the solution using AWS Athena.
6. Navigate to AWS Athena, and choose the workgroup that was created by the solution. You can find the workgroup name in the project's workflow.
7. Once in AWS Athena, navigate to _Saved Queries_. A sample query displays. Choose the sample query and choose _Run_. The Parquet data displays in AWS Athena.

## Customizing the solution

- If you would like to consume a different CSV schema, you can modify the AWS Glue table schema within the _lib/cdk-glue-etl-stack.ts_ file.
- If you would like to modify the AWS Glue job, you can modify the AWS Glue job code within the _lambdas/glue-csv-to-parquet.py_ file.
- If you would like to modify the Amazon S3 trigger, you can modify the Amazon S3 trigger code within the _lambdas/s3-notify-glue.py_ file.
- If you would like to modify the AWS CDK stack, you can modify the _lib/cdk-glue-etl-stack.ts_ file.
- If you would like to modify the Amazon CodeCatalyst workflow, you can modify the _.codecatalyst/deploymentPipeline.yaml_ file.

## AWS CDK application

The [AWS Cloud Development Kit (CDK)](https://aws.amazon.com/cdk/) allows you to accelerate cloud development using common programming languages to model your applications. It is an example of infrastructure as code (IaC). While CDK support several languages, this CDK application is written in [TypeScript](https://www.typescriptlang.org/). For more information on AWS CDK and TypeScript, see https://cdkworkshop.com/. The CDK application is built using [npm](https://www.npmjs.com/). There are several ways to install NPM and Node.js, which NPM relies upon. See https://docs.npmjs.com/downloading-and-installing-node-js-and-npm for details.

Before running CDK, you will need to configure your workstation with your AWS credentials and AWS region. If you have the AWS CLI installed, you can run the following command to configure your workstation:

`aws configure`

Provide your AWS access key ID, secret access key, and default Region when prompted.

Alternatively, you can set the environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_DEFAULT_REGION to appropriate values.

Once your AWS credentials and region have been set, to build the AWS CDK application, run the following commands:

```console
npm install
cdk synth
```

While Amazon CodeCatalyst has been set up to automatically deploy the application, you can deploy it without using the CI/CD
pipeline. Make sure your AWS account is [cdk bootstrapped](https://docs.aws.amazon.com/cdk/v2/guide/bootstrapping.html) prior to running the following command to deploy the application:

```console
cdk deploy --all
```

To execute unit tests for this application, run the following command:

```console
npm test
```

## Useful commands

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `npm run test` perform the jest unit tests
- `cdk deploy` deploy this stack to your default AWS account/Region
- `cdk diff` compare the deployed stack with the current state
- `cdk synth` emits the synthesized AWS CloudFormation template

## Important files

Below is a description of important source code files.

| File name                      | Brief description                                                                                               |
| ------------------------------ | --------------------------------------------------------------------------------------------------------------- |
| lib/cdk-glue-etl-stack.ts      | CDK Stack for pipeline and application infrastructure                                                           |
| lambdas/glue-csv-to-parquet.py | Custom Python Lambda for processing data from CSV to Parquet                                                    |
| lambdas/s3-notify-glue.py      | Custom Python Lambda used to trigger an AWS Glue job when a CSV file is uploaded to the Amazon S3 bucket        |
| sample-data/taxi_data.csv      | Sample CSV data. Upload this to the "csv-input" folder within the Amazon S3 bucket to trigger the full ETL flow |
| package.json                   | Dependencies for CDK                                                                                            |
