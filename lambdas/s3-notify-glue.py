# Import Boto 3 for AWS Glue
import boto3
import os
import urllib.parse

client = boto3.client('glue')


def handler(event, context):
    # Get the object from the event and show its content type
    bucketName = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    glueJobName = os.environ['JOB_NAME']
    response = client.start_job_run(
        JobName=glueJobName,
        Timeout=10,
        Arguments={
        '--s3_input_file_path': 's3://' + bucketName + '/' + key,
        '--s3_parquet_output_folder': 's3://' + bucketName + '/parquet-output/'
        }
    )
    return response
