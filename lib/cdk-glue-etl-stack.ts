import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import { aws_glue, aws_iam, aws_lambda, aws_s3_deployment, aws_s3_notifications, Stack } from "aws-cdk-lib";
import * as glue from "@aws-cdk/aws-glue-alpha";
import { aws_athena as athena } from "aws-cdk-lib";
import { Source } from "aws-cdk-lib/aws-s3-deployment";

export class CdkGlueEtlStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Name of the stack
    const stackName = cdk.Stack.of(this).stackName;

    /**
     * S3 Bucket Creation
     */

    // S3 bucket name, must be unique
    const s3BucketName = "sample-etl-bucket-eb1yi";

    // S3 Bucket to host glue scripts
    const bucket = new cdk.aws_s3.Bucket(this, "GlueJobBucket", {
      bucketName: s3BucketName,
      versioned: false,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      autoDeleteObjects: true,
      blockPublicAccess: cdk.aws_s3.BlockPublicAccess.BLOCK_ALL,
    });

    // Create empty input directory
    new aws_s3_deployment.BucketDeployment(this, "EmptyDirectory", {
      sources: [Source.data("csv-input/.keep", "")],
      include: ["csv-input/.keep"],
      prune: false,
      destinationBucket: bucket,
    });

    // Sync local scripts folder with S3 bucket
    new aws_s3_deployment.BucketDeployment(this, "DeployGlueScripts", {
      sources: [aws_s3_deployment.Source.asset("./lambdas")],
      destinationBucket: bucket,
      include: ["lambdas/*.py"],
      prune: false,
      destinationKeyPrefix: "lambdas",
    });

    /**
     * Glue Database, Table, and Job Creation
     */

    // Glue job execution IAM role
    const glue_role = new cdk.aws_iam.Role(this, "GlueRole", {
      assumedBy: new cdk.aws_iam.ServicePrincipal("glue.amazonaws.com"),
      managedPolicies: [cdk.aws_iam.ManagedPolicy.fromAwsManagedPolicyName("service-role/AWSGlueServiceRole")],
    });

    // Grant read write access for glue execution IAM role for S3 bucket
    bucket.grantReadWrite(glue_role);

    // Glue-ETL  job
    const glueCsvToParquetJob = new aws_glue.CfnJob(this, "GlueJob", {
      name: `${stackName}-glue-csv-to-parquet`,
      role: glue_role.roleArn,
      command: {
        name: "glueetl",
        scriptLocation: "s3://" + bucket.bucketName + "/lambdas/glue-csv-to-parquet.py",
      },
      glueVersion: "3.0",
    });
    // Create Glue database
    const glueDatabase = new glue.Database(this, "GlueDatabase", {
      databaseName: `${stackName}-glue-etl-database`,
    });

    // Create Glue table
    const glueTable = new glue.Table(this, "GlueTable", {
      database: glueDatabase,
      bucket: bucket,
      s3Prefix: "parquet-output/",
      tableName: "taxi_zone_lookup",
      columns: [
        {
          name: "LocationID",
          type: glue.Schema.STRING,
        },
        {
          name: "Borough",
          type: glue.Schema.STRING,
        },
        {
          name: "Zone",
          type: glue.Schema.STRING,
        },
        {
          name: "service_zone",
          type: glue.Schema.STRING,
        },
      ],
      partitionKeys: [],
      dataFormat: glue.DataFormat.PARQUET,
      enablePartitionFiltering: true,
    });

    /**
     * S3 upload notification -> Lambda -> Trigger Glue job
     */

    // Lambda function to trigger Glue job
    const s3NotifyGlueLambda = new aws_lambda.Function(this, "GlueHandler", {
      functionName: `${stackName}-GlueHandler`,
      runtime: aws_lambda.Runtime.PYTHON_3_10,
      code: aws_lambda.Code.fromAsset("lambdas"),
      handler: "s3-notify-glue.handler",
      environment: {
        JOB_NAME: glueCsvToParquetJob.name as string,
      },
    });

    // Create trigger for Lambda function using suffix
    const notification = new aws_s3_notifications.LambdaDestination(s3NotifyGlueLambda);
    notification.bind(this, bucket);

    // Add Create Event
    bucket.addObjectCreatedNotification(notification, {
      suffix: ".csv",
    });

    // Add Inline Policy to Lambda to provide access to start the jobs
    const accountID = Stack.of(this).account;
    const region = Stack.of(this).region;
    const customPolicy = new aws_iam.PolicyStatement({
      actions: ["glue:StartJobRun"],
      resources: [`arn:aws:glue:${region}:${accountID}:job/${glueCsvToParquetJob.name}`],
    });

    // Attach the policy to the Lambda role
    s3NotifyGlueLambda.role?.attachInlinePolicy(
      new aws_iam.Policy(this, "GluePolicy", {
        statements: [customPolicy],
      })
    );

    /**
     * Athena Sample Workgroup & Query
     */

    // Create Athena workgroup
    const sampleAthenaWorkGroup = new athena.CfnWorkGroup(this, "SampleAthenaWorkGroup", {
      name: `${stackName}-sample-athena-query`,
      recursiveDeleteOption: true,
      // the properties below are optional
      description: "description",
      state: "ENABLED",
      tags: [
        {
          key: "key",
          value: "value",
        },
      ],
      workGroupConfiguration: {
        resultConfiguration: {
          outputLocation: `s3://${s3BucketName}/athena-results`,
        },
        engineVersion: {
          selectedEngineVersion: "Athena engine version 3",
        },
      },
    });

    // Sample Athena query
    const sampleQuery = new athena.CfnNamedQuery(this, "SampleAthenaQuery", {
      database: glueDatabase.databaseName,
      queryString: `SELECT * FROM "AwsDataCatalog"."${stackName}-glue-etl-database"."taxi_zone_lookup" limit 10;`,

      // the properties below are optional
      name: `${stackName}-sample-athena-query`,
      workGroup: sampleAthenaWorkGroup.name,
    });
    sampleQuery.node.addDependency(sampleAthenaWorkGroup);

    /**
     * CDK Output
     */

    // CDK Outputs
    new cdk.CfnOutput(this, "S3NotifyGlueLambdaFunctionName", {
      value: s3NotifyGlueLambda.functionName,
    });
    new cdk.CfnOutput(this, "GlueJobName", {
      value: glueCsvToParquetJob.name as string,
    });
    new cdk.CfnOutput(this, "S3BucketName", { value: bucket.bucketName });
  }
}
