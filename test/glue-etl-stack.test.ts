import { Capture, Match, Template } from "aws-cdk-lib/assertions";
import * as cdk from "aws-cdk-lib";
import { CdkGlueEtlStack } from "../lib/cdk-glue-etl-stack";

describe("GlueETLStack", () => {
  const app = new cdk.App();
  const glueETLStack = new CdkGlueEtlStack(app, "GlueETLStack");

  // Prepare the stack for assertions.
  const template = Template.fromStack(glueETLStack);

  test("creates the expected S3 resources", () => {
    template.hasResource("AWS::S3::Bucket", {});
    template.hasResource("AWS::S3::BucketPolicy", {});
    template.hasResource("Custom::S3AutoDeleteObjects", {});
    template.hasResource("Custom::S3BucketNotifications", {});
    template.hasResource("Custom::CDKBucketDeployment", {});
  });

  test("creates the expected Lambda resources", () => {
    template.hasResource("AWS::Lambda::Permission", {});
    template.hasResource("AWS::Lambda::Function", {});
  });

  test("creates the expected Glue resources", () => {
    template.hasResource("AWS::Glue::Job", {});
    template.hasResource("AWS::Glue::Database", {});
    template.hasResource("AWS::Glue::Table", {});
  });

  test("creates the expected Athena resources", () => {
    template.hasResource("AWS::Athena::WorkGroup", {});
  });

  test("creates the expected IAM resources", () => {
    template.hasResource("AWS::IAM::Role", {});
  });
});
